'''
19 задание
x – 1 куча
y – 2 куча
p – чей ход
'''


def f(x, y, p):
    if x + y >= 84 and p == 3:
        return True
    if x + y <= 84 and p == 3:
        return False
    if x + y >= 84:
        return False
    if p % 2 == 0: # если ход не наш
        return f(x + 1, y, p + 1) or f(x, y + 1, p + 1) or f(x * 2, y, p + 1) or f(x, y * 3, p + 1) # здесь какие ходы могут быть
    else: #если ход нащ
        return f(x + 1, y, p + 1) and f(x, y + 1, p + 1) and f(x * 2, y, p + 1) and f(x, y * 3, p + 1) # здесь какие ходы могут быть


for i in range(1, 68):  # Диапазон S
    if f(16, i, 0):  # 0 - ничей ход, 1 – Петя, 2 – Ваня, 3 – Петя и тд
        print(i)

