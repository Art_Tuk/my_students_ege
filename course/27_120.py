f = open("28133_B (1).txt")

a = f.read().splitlines()
a = a[1:]
a = list(map(int, a))

m = []
for i in range(120):
    m.append(0)

first = 0
second = 0

for x in a:
    d = x % 120
    if x < m[(120 - d) % 120] and x + m[(120 - d) % 120] > first + second:
        first = m[120 - d]
        second = x
    if x > m[d]:
        m[d] = x

print(first, second)