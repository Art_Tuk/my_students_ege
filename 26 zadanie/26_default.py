f = open("26_demo (5).txt")

a = f.read().splitlines()

s = int(a[0].split()[0])
n = int(a[0].split()[1])

a = a[1:]

a = list(map(int, a))
a.sort()


s1 = 0
count = 0 # users
last = 0
for x in a:
    if s1 + x <= s:
        count += 1
        last = x
        s1 += x

c = s - (s1 - last)
if c in a:
    print(count, c)
else:
    for x in a:
        if x <= c:
            last = x
    print(count, last)
