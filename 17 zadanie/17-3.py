f = open("17-3.txt")
a = f.read().splitlines()
a = list(map(int, a))

count = 0
min_sum = 10000000

for i in range(len(a) - 4):
    if a[i] > a[i + 1] > a[i + 2] > a[i + 3] and abs(a[i] - a[i + 3]) > 1000:
        count += 1
        if sum(a[i:i + 4]) < min_sum:
            min_sum = sum(a[i:i + 4])

print(count, min_sum)