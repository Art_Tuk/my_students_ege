def f(x):
    count = 0
    for i in range(2, 10):
        if x % i == 0:
            count += 1
    if count == 3:
        return True
    else:
        return False



count = 0
min = 10000000
for x in range(50001, 90001):
    if f(x):
        count += 1
        if x < min:
            min = x

print(count, min)