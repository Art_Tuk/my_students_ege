f = open()
a = f.read().splitlines()
a = list(map(int, a))

min_21 = 1000000000

for x in a:
    if x < min_21 and x % 21 == 0:
        min_21 = x

count = 0
max_sum = -1

for i in range(len(a) - 1):
    if a[i] % min_21 == 0 or a[i + 1] % min_21 == 0:
        count += 1
        if a[i] + a[i + 1] > max_sum:
            max_sum = a[i] + a[i + 1]

print(count, max_sum)
