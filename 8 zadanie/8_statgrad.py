import itertools

s = "НАСТЯ"
count = 0

for x in itertools.product(s, repeat=7):
    if x.count("Н") == 2 and x.count("А") >= 1:
        count += 1

print(count)