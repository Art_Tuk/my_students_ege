for x in range(1, 10000):
    s = x
    a = 0
    b = 0
    while x > 0:
        c = x % 10
        a += c
        if c > b:
            b = c
        x //= 10
    if a == 11 and b == 7:
        print(s)