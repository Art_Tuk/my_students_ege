f = open("27-B (4).txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

first = [2 * (10 ** 9) for i in range(30)]  # 30 остатков от 0 до 29
max_sum = 0
count = 0  # счетчик положительных нечетных чисел
sum = 0

for x in a:
    sum += x
    if x % 2 != 0 and x > 0:
        count += 1
    k = count % 30
    if first[k] == 0:
        first[k] = sum
    else:
        if sum - first[k] > max_sum:
            max_sum = sum - first[k]

print(max_sum)