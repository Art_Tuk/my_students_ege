f = open("trash.txt")
a = f.read().splitlines()[1:]
a = list(map(int, a))

max_1 = 0
max_2 = 0
max_0 = 0
max_0_prev = 0

for x in a:
    if x % 3 == 0 and x >= max_0:
        max_0_prev = max_0
        max_0 = x
    elif x % 3 == 0 and max_0_prev < x < max_0:
        max_0_prev = x
    elif x % 3 == 1 and x > max_1:
        max_1 = x
    elif x % 3 == 2 and x > max_2:
        max_2 = x

print(max_0, max_0_prev, max_1, max_2)