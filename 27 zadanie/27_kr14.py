f = open("27985_A.txt")

a = f.read().splitlines()

a = a[1:]

a = list(map(int, a))

max_14 = 0
max_7 = 0
max_2 = 0
max = 0

for x in a:
    if x % 7 == 0 and x > max_7 and x % 2 != 0:
        max_7 = x
    if x % 2 == 0 and x > max_2 and x % 7 != 0:
        max_2 = x

    if x % 14 == 0 and x > max_14:
        max_14 = x
    elif x == max_14:
        max = max_14

    if x > max:
        max = x


if max * max_14 > max_2 * max_7:
    print(max * max_14)
else:
    print(max_7 * max_2)

