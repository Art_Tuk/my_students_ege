'''
https://inf-ege.sdamgia.ru/problem?id=28128
'''
f = open("28128_B.txt")

a = f.read().splitlines()

a = a[1:]

a = list(map(int, a))

max_3_0 = 0
max_3_1 = 0
max_1 = 0
max_2 = 0

for x in a:
    if x % 3 == 2 and x > max_2:
        max_2 = x
    elif x % 3 == 1 and x > max_1:
        max_1 = x
    elif x % 3 == 0 and x > max_3_0:
        max_3_1 = max_3_0 # 30 - на второе место
        max_3_0 = x # 45 - на первое
    elif x % 3 == 0 and max_3_1 < x < max_3_0:
        max_3_1 = x

if max_1 + max_2 > max_3_1 + max_3_0:
    print(max_1 + max_2)
else:
    print(max_3_1 + max_3_0)