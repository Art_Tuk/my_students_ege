f = open("27-B (2).txt")
a = f.read().splitlines()[1:]

first = 0
second = 0
third = 0
dif = []

for x in a:
    k, l, m = map(int, x.split())
    third += max(k, l, m)
    second += min(k, l, m)
    first += k + l + m - max(k, l, m) - min(k, l, m)
    if abs(k - m) < abs(l - m) and abs(k - m) % 2 != 0:
        dif.append(abs(k - m))
    elif abs(l - m) < abs(k - m) and abs(l - m) % 2 != 0:
        dif.append((abs(k - m)))

dif.sort()
print((first - dif[0]) % 2, second % 2, third - dif[0])
