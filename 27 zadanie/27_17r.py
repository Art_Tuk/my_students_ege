f = open("27991_B.txt")

a = f.read().splitlines()

a = a[1:]

a = map(int, a)

max_chet = 0
max_nechet = 0

max_17_chet = []
max_17_nechet = []

for x in a:
    if x % 17 == 0:
        if x % 2 == 0:
            max_17_chet.append(x)
        else:
            max_17_nechet.append(x)
    else:
        if x % 2 == 0 and x > max_chet:
            max_chet = x
        elif x % 2 == 1 and x > max_nechet:
            max_nechet = x


max_17_nechet.sort(reverse=True)
max_17_chet.sort(reverse=True)

print(max_nechet)
print(max_chet)
print(max_17_nechet[:10])
print(max_17_chet[:10])