f = open("17 (17).txt")
a = f.read().splitlines()
a = list(map(int, a))

count = 0
max_sum = -10000000

for i in range(len(a) - 1):
    if a[i] % 3 == 0 or a[i + 1] % 3 == 0:
        count += 1
        if a[i] + a[i + 1] > max_sum:
            max_sum = a[i] + a[i + 1]

print(count, max_sum)