import itertools

s = sorted("батыр")

index = 1
for x in itertools.product(s, repeat=5):
    a = ''.join(x)
    if a.count("ы") == 0 and "аа" not in a:
        print(index, a)
    index += 1