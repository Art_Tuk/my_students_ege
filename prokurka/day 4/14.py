x = 25

print(bin(x))
print(oct(x))
print(hex(x))

s = ""
while x > 0:
    s += str(x % 16)
    x //= 15
s = s[::-1]
print(s)
