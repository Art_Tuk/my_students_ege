import itertools

s = "георгий"

count = set()
for x in itertools.permutations(s):
    a = ''.join(x)
    if "гг" not in a:
        count.add(a)

print(len(count))
