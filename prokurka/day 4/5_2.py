def f(x):
    s = bin(x)[2:]
    first = s[:2][::-1]
    s += first
    return int(s, 2)


for n in range(1000):
    if f(n) > 74:
        print(n)
        break