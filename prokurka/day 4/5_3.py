def f(x):
    ost = x % 3
    s = ""
    while x > 0:
        s += str(x % 3)
        x //= 3
    s = s[::-1]
    s += str(ost)
    return int(s, 3)


for x in range(10000):
    if f(x) >= 1000:
        print(f(x))
        break
