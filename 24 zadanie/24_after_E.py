'''
Определите символ, который чаще всего встречается в файле после E.
'''
f = open("24 (23).txt")

a = f.read()

count = []

for x in range(96):
    count.append(0)


for i in range(len(a) - 1):
    if a[i] == "E":
        index = ord(a[i + 1]) # число из буквы - его уникальный id
        count[index] += 1
print(count)
max = 0
max_index = 0 

for i in range(len(count)):
    if count[i] > max:
        max = count[i]
        max_index = i

print(chr(max_index))
