import collections


text = open("24-s1.txt").read().splitlines()

min_s = ""
min_count = 100000
res = []

for string in text:
    count = 0
    for i in range(len(string) - 1):
        if ord(string[i]) == ord(string[i + 1]) - 1:
            count += 1
    if count < min_count:
        min_count = count
        min_s = string



print(min_count)
print(collections.Counter(min_s))

text = ''.join(text)

print(text.count("W"))