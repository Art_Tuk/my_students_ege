from collections import Counter

f = open("24 (19).txt")

a = f.read()

count = []

for i in range(1, len(a) - 1):
    if a[i - 1] == a[i + 1]:
        count.append(a[i])

print(Counter(count))