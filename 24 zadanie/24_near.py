f = open()

a = f.read()

count_max = 0
count = 1

"""
AAABBB
012345
len(a) = 6
"""

for i in range(0, len(a) - 1):
    if a[i] != a[i + 1]:
        count += 1
        if count > count_max:
            count_max = count
    else:
        count = 1

print(count_max)