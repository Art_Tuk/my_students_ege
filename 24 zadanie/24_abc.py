f = open("24 (1).txt")

a = f.read().splitlines()

min_count = 1000000000
min_str = ""
for x in a:
    if x.count("N") < min_count:
        min_count = x.count("N")
        min_str = x

count = []
for i in range(1000):
    count.append(0)

for i in range(len(min_str)):
    count[ord(min_str[i])] += 1

max_count = -1
max_index = -1
for i in range(len(count)):
    if count[i] >= max_count:
        max_count = count[i]
        max_index = i

print(chr(max_index))
