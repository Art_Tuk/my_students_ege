from math import sqrt


def simple(x):
    for n in range(2, x):
        if x % n == 0:
            return False
    return True


def f(x):
    while x % 2 == 0:
        x //= 2
    if sqrt(x) == int(sqrt(x)):
        s = int(sqrt(x))
        if sqrt(s) == int(sqrt(s)):
            p = int(sqrt(s))
            if simple(p):
                return True


for x in range(35000000, 40000001):
    if f(x):
        print(x)