from math import sqrt


def f(x):
    d = []
    for i in reversed(range(2, x)):
        if x % i == 0:
            d.append(i)
            if len(d) == 2:
                return d
    return d


for x in range(10_000_001, 15_000_000):
    s = f(x)
    if len(s) == 2:
        s = sum(s)
        if s < 100000 and s % 31 == 0:
            print(x, s)