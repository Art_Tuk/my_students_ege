import re
from functools import lru_cache



@lru_cache()
def f(x):
    if x % 17 == 0 and re.search("1[0-9]34567[0-9]9", str(x)):
        return x // 17


for x in range(103456709, 10345670900):
    if f(x):
        print(x, f(x))

