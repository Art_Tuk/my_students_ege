from math import sqrt

def simple(x):
    for i in range (2, x):
        if x % i == 0:
            return False
    return True


def f(x):
    while x % 2 == 0:
        x //= 2
    if sqrt(x) == int(sqrt(x)):
        y = int(sqrt(x))
        if sqrt(y) == int(sqrt(y)) and simple(int(sqrt(y))):
            return True
    return False


for x in range(35000000, 40000000):
    if f(x):
        print(x)
