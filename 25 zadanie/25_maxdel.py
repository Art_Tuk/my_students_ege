def count(x):
    n = 0
    for i in range(1, x + 1):
        if x % i == 0:
            n += 1
    return n


max_number = 0
max_count = 0
for x in range(84052, 84130 + 1):
    if count(x) > max_count:
        max_count = count(x)
        max_number = x

print(max_count, max_number)
