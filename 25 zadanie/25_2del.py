"""
count(6) -> [2, 3]
count(8) -> [2, 4]
"""


def count(x):
    res = []
    for i in range(2, x):
        if x % i == 0:
            res.append(i)
    if len(res) == 2:
        return res


for x in range(174457, 174505 + 1):
    if count(x):
        print(count(x))