
def c(x):
    count = 0
    for i in range(1, x + 1):
        if x % i == 0:
            count += 1
    return count

min_num = 10000000000000
min_count = 0

for x in range(700000, 1_000_000_000):
    if c(x) > min_count:
        min_count = c(x)
        min_num = x
        print(x, min_count)
