def f(x):
    count = 0
    for i in range(1, x + 1):
        if x % i == 0:
            count += 1
    return count

max_count = -1
max_num = -1

for x in range(700000, 10_000_000):
    if f(x) > max_count:
        print(x, f(x))
        max_count = f(x)
