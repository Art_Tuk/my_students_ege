import re
from functools import lru_cache


@lru_cache()
def simple(x):
    for i in range(2, x):
        if x % i == 0:
            return False
    return True


@lru_cache()
def f(x):
    s = ""
    max_del = 0
    for i in range(2, x):
        if x % i == 0 and simple(i):
            s += str(i)
            max_del = i
    if re.search("27[0-9]*39[0-9]", s) or re.search("34[0-9]*2[0-9]7", s):
        return max_del


for x in range(4679000 + 1, 4679000 + 1000000):
    if f(x):
        print(x, f(x))

