def f(x):
    m = 0
    for n in range(2, x):
        if x % n == 0:
            m += n
            m += x // n
            break
    if m % 7 == 3:
        return m


for x in range(452021 + 1, 100000000):
    if f(x):
        print(x, f(x))
